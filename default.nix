# Reproductibility: pin a specific commit of nur-kapack repository
# This one contains Batsim v4.2 and Simgrid v3.34
{ kapack ? import
    (fetchTarball "https://github.com/oar-team/nur-kapack/archive/901a5b656f695f2c82d17c091a55db2318ed3f39.tar.gz")
    # commit 02/08/23 "batsim: 4.2.0"
  {}
}:

with kapack.pkgs;

let self = rec {
  # For my scheduler and user simulator (batmen), pin a specific git commit:
  batmen-31 = kapack.batsched.overrideAttrs (attr: rec {
    name = "batmen";
    version = "refs/tags/v3.1";
    src = kapack.pkgs.fetchgit rec {
      url = "https://gitlab.irit.fr/sepia-pub/mael/batmen.git";
      rev = version;
      sha256 = "sha256-9wX9gKl4ANt6fL6saZRRRajbY1E24soGlmIZNEWg7sU=";
    };
  });

  exp_env = mkShell rec {
    name = "exp_env";
    buildInputs = [
      kapack.batsim-420   # data center simulator
      batmen-31           # scheduler + user simulator
      kapack.batexpe      # tool to execute batsim instances
      kapack.evalys       # for data visualization
      (python3.withPackages 
        (ps: with ps; with python3Packages; [jupyter ipython pandas numpy matplotlib
          plotly pip tabulate pytz isodate ordered-set yattag])
      )
      wget
    ];
  };
};
in
  self



#!/usr/bin/env python3

import random
# from time import *
import datetime
from dateutil import parser
import concurrent.futures

from scripts.util import WL_DIR
from instance import start_instance

###############################
# Prepare the start date sample
###############################
begin_trace = 1356994806  # according to original SWF header
jun1 = parser.parse('Sun Jun  1 00:00:00 CEST 2014')
daylight_saving_day = parser.parse('Sun Oct 26 00:00:00 CEST 2014')
day = datetime.timedelta(days=1)
weekdays = [1, 2, 3, 4, 5] # Mon to Fri

# We do one expe for every weekday beween Jun 1 and Oct 26
expe_start_time = []
str_start_day = []
day1 = jun1
while day1 <= (daylight_saving_day - 3*day):
    day2 = day1 + day
    if day2.isoweekday() in weekdays:
        str_start_day.append(day1.ctime())
        expe_start_time.append(day1.timestamp() - begin_trace)
    day1 += day

# Save slected dates in a txt file
with open(f"{WL_DIR}/start_days_for_campaign2.txt", 'w') as f:
    for date in str_start_day:
        f.write(date + '\n')

###############################
# Launch the expe for every start date
###############################
nb_expe = len(expe_start_time)

with concurrent.futures.ProcessPoolExecutor() as executor:
    instances = []
    for i in range(nb_expe):
        print(f"Submit expe {i}")
        # start_instance(expe_num, start_date, prepare_workload, clean_log)
        instances.append(executor.submit(
            start_instance, i, expe_start_time[i], True, True))

    for instance in concurrent.futures.as_completed(instances):
        print(f"Expe {instance.result()} terminated")

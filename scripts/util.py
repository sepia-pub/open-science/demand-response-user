#!/usr/bin/env python3
import os
import os.path
import subprocess
import numpy as np
import pandas
from matplotlib import figure, pyplot as plt
from evalys.jobset import JobSet
from evalys.metrics import compute_load
import evalys.visu.legacy as vleg

############
# USEFUL VAR
############
ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
WL_DIR = f'{ROOT_DIR}/workload'

############
# TO RUN EXPES WITH ROBIN
############
class RobinInstance(object):
    def __init__(self, output_dir, batcmd, schedcmd, simulation_timeout, ready_timeout, success_timeout, failure_timeout):
        self.output_dir = output_dir
        self.batcmd = batcmd
        self.schedcmd = schedcmd
        self.simulation_timeout = simulation_timeout
        self.ready_timeout = ready_timeout
        self.success_timeout = success_timeout
        self.failure_timeout = failure_timeout

    def to_yaml(self):
        # Manual dump to avoid dependencies
        return f'''output-dir: '{self.output_dir}'
batcmd: "{self.batcmd}"
schedcmd: "{self.schedcmd}"
simulation-timeout: {self.simulation_timeout}
ready-timeout: {self.ready_timeout}
success-timeout: {self.success_timeout}
failure-timeout: {self.failure_timeout}
'''

    def to_file(self, filename):
        create_dir_rec_if_needed(os.path.dirname(filename))
        write_file(filename, self.to_yaml())

def gen_batsim_cmd(platform, workload, output_dir, more_flags):
    return f"batsim -p '{platform}' -w '{workload}' -e '{output_dir}/' {more_flags}"

def write_file(filename, content):
    file = open(filename, "w")
    file.write(content)
    file.close()

def create_dir_rec_if_needed(dirname):
    if not os.path.exists(dirname):
            os.makedirs(dirname)

def run_robin(filename):
    return subprocess.run(['robin', filename])

############
# FOR DATA VIZ
############
class JobSetMulticore(JobSet):
    """Custom version of jobset to change the way 'utilisation' is computed."""
    def __init__(self, df, resource_bounds=None, float_precision=6):
        JobSet.__init__(self, df, resource_bounds, float_precision)
        self.MaxProcs = len(self.res_bounds) * 16 # because my machines have 16 cores

    @property # override
    def utilisation(self):
        if self._utilisation is not None:
            return self._utilisation
        self._utilisation = compute_load(self.df, col_begin='starting_time', col_end='finish_time',
                                         col_cumsum='requested_number_of_resources') # original: proc_alloc
        return self._utilisation
    
def plot_load_and_details(expe_file, export_suffix="png", dpi=1000):
    begin, end = 24 * 3600, 48 * 3600

    js = JobSetMulticore.from_csv(expe_file + "/_jobs.csv")
    #js.df = js.df[(js.df.submission_time >= begin) & (js.df.submission_time < end)]
    fig, axe = plt.subplots(nrows=2, sharex=True, figsize=(16, 8), tight_layout=True)
    fig.suptitle(expe_file, fontsize=16)
    vleg.plot_load(js.utilisation, js.MaxProcs, time_scale=False, ax=axe[0])
    vleg.plot_job_details(js.df, js.MaxProcs, time_scale=False, ax=axe[1])

    for ax in axe:
        ax.xaxis.set_ticks(np.arange(begin, end, 2*3600))
        ax.xaxis.set_ticklabels(np.arange(24, 48, 2))

    plt.xlim(begin, end)
    fig.savefig(f"{expe_file}_viz.{export_suffix}", dpi=dpi)
    plt.show()
    plt.close(fig)

def energy_consumed_in(window, OUT_DIR):
    """Return the energy consumed during the time window (in kWh)."""
    # The problem is that batsim time series don't always have the specific
    # timestamp, we need to extrapolate
    data = pandas.read_csv(OUT_DIR + "/_consumed_energy.csv")
    energy = [0, 0]
    for i in range(2):
        first_greater = data[data['time'].ge(window[i])].index[0]
        df_entry = data.iloc[first_greater]
        energy[i] = df_entry['energy']
        delta_energy = (df_entry['time'] - window[i]) * df_entry['epower']
        energy[i] -= delta_energy

    return (energy[1] - energy[0]) / 3600 / 1000


def scheduling_metrics_in(window, OUT_DIR):
    """Return the usual scheduling metrics for the subpart of jobs that have their submission time within the time window."""
    [inf, sup] = window
    data = pandas.read_csv(OUT_DIR + "/_jobs.csv")
    data_in_window = data[(data.submission_time
                           >= inf) & (data.submission_time <= sup)]

    out = {}
    out["makespan"] = data_in_window["finish_time"].max() - inf
    out["#jobs"] = len(data_in_window.index)
    out["mean_waiting_time"] = data_in_window["waiting_time"].mean()
    out["max_waiting_time"] = data_in_window["waiting_time"].max()
    out["mean_slowdown"] = data_in_window["stretch"].mean()
    out["max_slowdown"] = data_in_window["stretch"].max()

    return out
#/bin/bash
cd ~/demand-response-user
nix-shell --pure -A exp_env --run "jupyter notebook --ip $(hostname -f) --no-browser"

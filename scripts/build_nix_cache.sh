# To save the nix cache in a file, for future use
# (useful when working in Grid'5000)
nix-store --query --references $(nix-instantiate default.nix) | \
    xargs nix-store --realise | \
    xargs nix-store --query --requisites | \
    xargs nix-store --export > cache_nix


#nix-store --export $(nix-store -qR paths) > cache_nix

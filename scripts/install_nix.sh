# /bin/bash
cd ~/demand-response-user
sudo-g5k
sudo su root --command "echo 1 > /proc/sys/kernel/unprivileged_userns_clone"
curl https://nixos.org/releases/nix/nix-2.6.0/install | sh
source ${HOME}/.nix-profile/etc/profile.d/nix.sh

# Uncomment the line below if you want to import the nix cache from a file
# (useful when working in Grid'5000)

# nix-store --import < cache_nix
